package com.village.habitants;

import com.village.history.Day;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.shadow.com.univocity.parsers.csv.CsvParser;

import java.beans.BeanProperty;

import static org.junit.jupiter.api.Assertions.*;

// https://www.baeldung.com/parameterized-tests-junit-5
class HabitantTest {


    public static Habitant habitant;

    @BeforeEach
    public void beforeTest(){
        System.out.println("beforeClass!!");
        habitant = new Habitant(new Day());
    }

    @AfterAll
    public static void afterClass(){
        System.out.println("afterClass!!");
        Habitant.fullNames = null;
    }

    @ParameterizedTest
    @CsvSource({"Ignat, Snaip, bird",
            "Orlando, Snow, fly"})
    void fullName(String name, String surname, String surname2) {
        habitant.name = name;
        habitant.surname1 = surname;
        habitant.surname2 = surname2;
        String expectValue = habitant.name + " " + habitant.surname1 + habitant.surname2;
        String actualValue = habitant.fullName();
        assertEquals(expectValue, actualValue);
    }

    @Test
    @RepeatedTest(10)
    void veryFullName() {
        assert (true);
    }

    @Test
    @Tag("payment")
    @Disabled
    void die() {
        assert (false);
    }
}
