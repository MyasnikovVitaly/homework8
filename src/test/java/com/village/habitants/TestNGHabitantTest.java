/*
package com.village.habitants;

import com.village.history.Day;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.annotations.DataProvider;

import java.util.Collections;


// https://www.baeldung.com/parameterized-tests-junit-5
public class TestNGHabitantTest {

    public static Habitant habitant;

    @DataProvider(name = "testData")
    public Object[][] data(){
        return new Object[][] {
                {"Ignat", "Snaip", "bird"}, {"Orlando", "Snow", "fly"}
        };
    }

    @BeforeMethod
    public void beforeTest(){
        System.out.println("beforeClass!!");
        habitant = new Habitant(new Day());
    }

    @AfterClass
    public static void afterClass(){
        System.out.println("afterClass!!");
        Habitant.fullNames = null;
    }

    @Test(groups = "nameTest", dataProvider = "testData", testName = "FullName")
    public void fullNameTesting(String name, String surname, String surname2) {
        habitant.name = name;
        habitant.surname1 = surname;
        habitant.surname2 = surname2;
        String expectValue = habitant.name + " " + habitant.surname1 + habitant.surname2;
        String actualValue = habitant.fullName();
        Assert.assertEquals(expectValue, actualValue);
    }

    @Test(invocationCount = 100, groups = "smoke")
    public void uniqueNameTesting(){
        Assert.assertTrue(Collections.frequency(Habitant.fullNames, habitant.fullName()) == 1);
    }


    @Test(groups = "nameTest")
    public void veryFullName() {
        assert (true);
    }

    @Test(enabled = false)
    void die() {
        assert (false);
    }
}*/
