package com.village.events;

import com.village.exceptions.DawnException;
import com.village.exceptions.MurderException;
import com.village.exceptions.ShootoutException;
import com.village.habitants.Habitant;
import com.village.habitants.Role;
import com.village.history.Day;

import java.util.ArrayList;
import java.util.Random;

public class Visit {
    private Day day;
    private Habitant host;
    private Habitant guest;
    private Random random = new Random();
    public ArrayList<Habitant> survivors = new ArrayList<>();
    public Visit(Habitant host, Habitant guest, Day day) {
        this.host = host;
        this.guest = guest;
        this.day = day;
    }

    public Event outcome() throws MurderException, ShootoutException, DawnException {
        Event event = null;
        if (random.nextInt(100)<=49) {policemanLogic(host, guest);}
        if (day.witchDay) {witchLogic(host, guest);}
        if (day.fullMoon) {werewolfLogic(host, guest);}
        event = vampireLogic(host, guest);
        if (event != null) {
            return event;
        } else {
            survivors.add(host);
            survivors.add(guest);
            return new Event("visit", host.veryFullName() + "drinks tea with " + guest.veryFullName(), day);
        }
    }

    private void policemanLogic(Habitant host, Habitant guest) throws ShootoutException, MurderException {
        if ((guest.role == Role.POLICEMAN) && (host.role == Role.POLICEMAN)) {
            String message = guest.veryFullName() + "and " + host.veryFullName() + "killed each other";
            host.die();
            guest.die();
            throw new ShootoutException(message);
        }
        if ((host.role == Role.POLICEMAN) && (guest.role != Role.PEASANT) && (guest.role != Role.WITCH)) {
            murder(host, guest);
        }
        if ((guest.role == Role.POLICEMAN) && (host.role != Role.PEASANT) && (host.role != Role.WITCH)) {
            murder(guest, host);
        }
    }

    private void murder(Habitant witch, Habitant victim) throws MurderException {
        String message = victim.veryFullName() + "is killed by " + witch.veryFullName();
        victim.die();
        survivors.add(witch);
        throw new MurderException(message);
    }

    private void witchLogic(Habitant host, Habitant guest) throws MurderException {
        if ((host.role == Role.WITCH) && (guest.role != Role.WITCH)) {
            murder(host, guest);
        }
        if ((host.role != Role.WITCH) && (guest.role == Role.WITCH)) {
            murder(guest, host);
        }
    }

    private void werewolfLogic(Habitant host, Habitant guest) throws MurderException {
        if ((host.role == Role.WEREWOLF) && day.fullMoon) {
            murder(host, guest);
        }
        if ((guest.role == Role.WEREWOLF) && day.fullMoon) {
            murder(guest, host);
        }
    }

    private Event vampireLogic(Habitant host, Habitant guest) throws MurderException, DawnException {
        if (host.role == Role.VAMPIRE) {
            return vampireHouse(host, guest);
        }
        if (guest.role == Role.VAMPIRE) {
            return vampireHouse(guest, host);
        }
        return null;
    }

    private Event vampireHouse(Habitant vampire, Habitant victim) throws MurderException, DawnException {
        if (((victim.role == Role.PEASANT) || (victim.role == Role.WITCH)) && (random.nextInt(100)<=6)) {
            Event event = new Event("bite", victim.veryFullName() + "is bitten by " + vampire.veryFullName(), day);
            victim.role = Role.VAMPIRE;
            survivors.add(vampire);
            survivors.add(victim);
            return event;
        }
        if (random.nextInt(100)<=4) {
            murder(vampire, victim);
        }
        if (random.nextInt(100)==0) {
            String message = vampire.veryFullName() + "is killed by dawn at the house of " + victim.veryFullName();
            vampire.die();
            survivors.add(victim);
            throw new DawnException(message);
        }
        survivors.add(vampire);
        survivors.add(victim);
        return new Event("visit", victim.veryFullName() + "drinks tea with " + vampire.veryFullName(), day);
    }
}
