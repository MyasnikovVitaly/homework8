package com.village.events;

import com.village.history.Day;

public class Event {
    public String what;
    public String details;
    public Day day;
    public Event(String what, String details, Day day) {
        this.what = what;
        this.details = details;
        this.day = day;
    }

    @Override
    public String toString() {
        return day.dayName + ": " + details;
    }
}
