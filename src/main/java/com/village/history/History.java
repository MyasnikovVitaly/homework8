package com.village.history;


import com.village.events.Event;
import com.village.village.Village;

import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class History {
    private ArrayList<Day> days = new ArrayList<>();
    ArrayList<Event> events = new ArrayList<>();
    private long moonLoop = 2551442877L; //ms beetween full moons
    public History (int startYear, int finalYear, GregorianCalendar firstFullMoon) {
        GregorianCalendar calendar = new GregorianCalendar(startYear, 0, 1);
        boolean counting = false;
        long time = 2551442877L;
        while (calendar.get(Calendar.YEAR) != finalYear) {
            boolean fullmoon = false;
            boolean witchDay = false;
            calendar.add(Calendar.HOUR, 24);
            if (counting) {
                time += 24*1000*60*60;
            }
            if ((time >= moonLoop) || calendar.equals(firstFullMoon)) {
                time -= moonLoop;
                counting = true;
                fullmoon = true;
            }
            if (((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) && (calendar.get(Calendar.DAY_OF_MONTH) == 13)) ||
                    ((calendar.get(Calendar.DAY_OF_MONTH) == 31) && (calendar.get(Calendar.MONTH) == 10))){
                witchDay = true;
            }
            Day day = new Day();
            day.witchDay = witchDay;
            day.fullMoon = fullmoon;
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd (EE)");
            day.dayName = formater.format(calendar.getTime());
            days.add(day);
        }
    }

    public void generate(Village village) {
        for (Day day : days) {
            Event event = village.someEvent(day);
            events.add(event);
        }
    }

    public void print() {
        for (Event event : events) {
            System.out.println(event);
        }
    }

    public void printInteresting() throws UnsupportedEncodingException {
        PrintStream ps = new PrintStream(System.out, true, "UTF-8");

        for (Event event : events) {
            if ((!event.what.equals("nothing")) && (!event.what.equals("visit"))) {
                ps.println(event);
            }
        }
    }

    public void print(String path) throws IOException {
        Path realPath = Path.of(path);
        Files.deleteIfExists(realPath);
        Files.createFile(realPath);
        for (Event event : events) {
            Files.writeString(realPath, event.toString(), StandardOpenOption.APPEND);
        }
    }
}

