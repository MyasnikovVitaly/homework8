package com.village.habitants;

import com.village.history.Day;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Habitant {
    private static String[] GIRLS = {"Lisa", "Molly", "Clara", "Rachel", "Joan", "Jane", "Margaretha", "Lilly", "Rosa",
            "Maria", "Marina", "Nina", "Sara", "Helena", "Nancy", "Lola", "Martha", "Alisa", "Janet", "Tracy"};
    private static String[] BOYS = {"Mike", "John", "Donald", "Tom", "Carl", "Nick", "Ned", "Harry", "Frank", "Ben",
            "Lou", "Jake", "Andrew", "Rajesh", "Ronald", "Nevil", "Jose", "Hans", "Bruce", "Mark", "Alex", "Marty"};
    private static String[] FIRST = {"Fast", "White", "Green", "Black", "Blue", "Short", "Long", "Red", "Large",
            "Small", "Bright", "Blind", "Deep", "Light", "Dark", "Slow", "Drunk", "Bon", "Mad", "Good", "Bad", "Snow"};
    private static String[] SECOND = {"leg", "man", "field", "bird", "dog", "fly", "neck", "cheek", "bottom", "son",
            "dream", "moore", "foot", "gun", "", "ton", "bride", "plate", "forest", "gump", "water", "runner",};
    private static ArrayList<String> girlNamePool = new ArrayList<>(Arrays.asList(GIRLS));
    private static ArrayList<String> boyNamePool = new ArrayList<>(Arrays.asList(BOYS));
    private static ArrayList<String> surnamePool1 = new ArrayList<>(Arrays.asList(FIRST));
    private static ArrayList<String> surnamePool2 = new ArrayList<>(Arrays.asList(SECOND));

    public static ArrayList<String> fullNames= new ArrayList<>();
    public String name;
    public String surname1;
    public String surname2;

    public Role role = Role.random();
    public Gender gender = Gender.random();
    public Day birth;
    private Random random = new Random();

    public Habitant(Day day) {
        generateName();
        birth = day;
    }
    // 4 = 1*2*3* 4 * 5

    private String generateName() {
        ArrayList<String> names = (gender.equals(Gender.MALE)) ? girlNamePool : boyNamePool;
        name = names.get(random.nextInt(names.size()));
        surname1 = surnamePool1.get(random.nextInt(surnamePool1.size()));
        surname2 = surnamePool2.get(random.nextInt(surnamePool1.size()));
        if (fullNames.indexOf(fullName())>=0) {
            return generateName();
        } else {
            fullNames.add(fullName());
            return fullName();
        }
    }

    public String fullName() {
        return name + " " + surname1 + surname2;
    }

    public String veryFullName() {
        return fullName() + " (" + role.name + ") ";
    }

    public void die() {
        fullNames.remove(fullName());
    }
}
