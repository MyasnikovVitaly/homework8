package com.village.habitants;

import java.util.Random;

public enum Gender {
    MALE, FEMALE;
    private static Random rand = new Random();
    public static Gender random(){ return values()[rand.nextInt(values().length)];}
}
