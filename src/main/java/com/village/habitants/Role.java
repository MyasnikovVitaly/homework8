package com.village.habitants;

import java.util.Random;

public enum Role {
    VAMPIRE ("vampire"),
    PEASANT ("peasant"),
    WITCH ("witch"),
    WEREWOLF ("werewolf"),
    POLICEMAN ("policeman");
    public String name;
    Role(String what) {
        name = what;
    }
    private static Random rand = new Random();
    public static Role random(){ return values()[rand.nextInt(values().length)];}
}
