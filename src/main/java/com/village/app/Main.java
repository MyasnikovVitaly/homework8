package com.village.app;

import com.village.history.History;
import com.village.village.Village;

import java.io.UnsupportedEncodingException;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) throws UnsupportedEncodingException {
        Village village = new Village("Villariba");
        History history = new History(1650, 1750, new GregorianCalendar(1650,0,17));
        history.generate(village);
        history.printInteresting();
        System.out.println("____________");
        village.population();
    }
}
