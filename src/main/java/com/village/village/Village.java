package com.village.village;


import com.village.events.Event;
import com.village.events.Visit;
import com.village.exceptions.DawnException;
import com.village.exceptions.MurderException;
import com.village.exceptions.ShootoutException;
import com.village.habitants.Habitant;
import com.village.history.Day;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Random;

public class Village {
    private ArrayList<Habitant> habitants = new ArrayList<>();
    private Random random = new Random();
    private String name;
    private Day day;
    private int deathToll = 0;

    public Village(String name) {
        this.name = name;
    }

    public void population() throws UnsupportedEncodingException {
        PrintStream ps = new PrintStream(System.out, true, "UTF-8");
           Integer i = 0;
        for (Habitant habitant : habitants) {
            i++;
            ps.println(i+ ". " + habitant.veryFullName() + "lives in " + name+ " since " +habitant.birth.dayName);
        }
        ps.println("FRAGS: "+ deathToll);
    }

    public Event someEvent(Day day) {
        this.day = day;
        if (random.nextInt(10) != 0) {
            return new Event("nothing", "Everybody sleeps. Nothing is happening", day);
        }
        if (random.nextInt(habitants.size()+1) <= 2) {
            return addHabitant();
        }
        if ((habitants.size() == 50) || (random.nextInt(habitants.size()) >= 48) ){
            return deleteHabitant();
        }
        return visit();
    }

    public Event addHabitant() {
        Habitant habitant = new Habitant(day);
        habitants.add(habitant);
        return new Event("new", habitant.veryFullName()  + "enters " + name, day);
    }

    public Event deleteHabitant() {
        Habitant habitant = habitants.remove(random.nextInt(habitants.size()));
        String message = habitant.veryFullName() + "leaves " + name;
        habitant.die();
        return new Event("leave", message, day);
    }

    public Event visit() {
        Event event = null;
        Habitant host = habitants.remove(random.nextInt(habitants.size()));
        Habitant guest = habitants.remove(random.nextInt(habitants.size()));
        Visit visit = new Visit(host, guest, day);
        try {
            event = visit.outcome();
        } catch (MurderException e) {
            event = new Event("murder", "MURDEREXCEPTION " + e.message, day);
            deathToll++;
        } catch (DawnException e) {
            event = new Event("dawn", "DAWNEXCEPTION " + e.message, day);
            deathToll++;
        } catch (ShootoutException e) {
            event = new Event("shootout", "SHOOTOUT " + e.message, day);
            deathToll+=2;
        }
        finally {
            habitants.addAll(visit.survivors);
            return event;
        }
    }
}
