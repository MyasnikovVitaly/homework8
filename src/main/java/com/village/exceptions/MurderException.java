package com.village.exceptions;

public class MurderException extends Exception{
    public String message;

    public MurderException(String message) {
        this.message = message;
    }
}