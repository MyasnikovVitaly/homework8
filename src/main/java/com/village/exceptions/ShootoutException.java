package com.village.exceptions;

public class ShootoutException extends Throwable {
    public String message;

    public ShootoutException(String message) {
        this.message = message;
    }
}
