package com.village.exceptions;

public class DawnException extends Exception{
    public String message;

    public DawnException(String message) {
        this.message = message;
    }
}